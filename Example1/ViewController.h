//
//  ViewController.h
//  Example1
//
//  Created by Nazifa Najish on 6/17/16.
//  Copyright © 2016 Techment DEV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVYSideMenuController.h"
#import "MVYMenuViewController.h"

@interface ViewController : UIViewController

- (IBAction)open:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITextView *textview;
@property (strong, nonatomic) IBOutlet UILabel *labeltext;

@end

