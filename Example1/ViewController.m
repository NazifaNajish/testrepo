//
//  ViewController.m
//  Example1
//
//  Created by Nazifa Najish on 6/17/16.
//  Copyright © 2016 Techment DEV. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    //MVYSideMenuController *sideMenuController;
}
@end

@implementation ViewController

@synthesize textview, labeltext;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    /*
    MVYMenuViewController *menuVC = [[MVYMenuViewController alloc] initWithNibName:@"MVYMenuViewController" bundle:nil];
    //MVYContentViewController *contentVC = [[MVYContentViewController alloc] initWithNibName:@"MVYContentViewController" bundle:nil];
   // UINavigationController *contentNavigationController = [[UINavigationController alloc] initWithRootViewController:contentVC];
    MVYSideMenuOptions *options = [[MVYSideMenuOptions alloc] init];
    options.contentViewScale = 1.0;
    options.contentViewOpacity = 0.05;
    options.shadowOpacity = 0.5;
    sideMenuController = [[MVYSideMenuController alloc] initWithMenuViewController:menuVC contentViewController:self
                                                                                            options:options];
    
//   sideMenuController = [[MVYSideMenuController alloc] init];
    
    sideMenuController.menuFrame = CGRectMake(0, 64.0, 220.0, 500);
    */
    //[self.view addSubview:sideMenuController.view];
    
    
    NSLog(@"Tested");

    
    self.navigationItem.title = @"MVYSideMenu!";
    [self addLeftMenuButtonWithImage:[UIImage imageNamed:@"menu_icon"]];

    CGFloat topCorrect = ([textview bounds].size.height - [textview contentSize].height);
    topCorrect = (topCorrect <0.0 ? 0.0 : topCorrect);
    textview.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
    
    
//    CGFloat topCorrect = ([labeltext bounds].size.height - [labeltext bounds].size.height);
//    topCorrect = (topCorrect <0.0 ? 0.0 : topCorrect);
//    labeltext.contentMode = uitextcontent

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)open:(UIButton *)sender {
    
    NSLog(@"method called");
   // [sideMenuController openMenu];
    
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController) {
        [sideMenuController openMenu];
    }
    
}
@end
