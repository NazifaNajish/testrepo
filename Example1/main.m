//
//  main.m
//  Example1
//
//  Created by Nazifa Najish on 6/17/16.
//  Copyright © 2016 Techment DEV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
